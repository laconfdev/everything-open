---
layout: post
title: "Introducing Everything Open"
---

<p class="lead">Linux Australia introduces a new grassroots open technologies conference starting in 2023, Everything Open.</p>

The past two years have taken their toll on open source communities within Australia.
We have seen communities around many projects go from holding regular meetups, sometimes multiple in the one city, to now being close to non-existent.
This is something that we were starting to see signs of prior to 2020, with a number of community group leaders looking to step back after having run meetups and other events for several years.
Unfortunately COVID-19 exacerbated this situation, preventing many people from running community events, which still have not recovered to where they previously were.

Our communities have always been very strong.
Linux Australia has helped many different groups over the years, and continues to provide support for the open source and open technologies communities within Australia.
We have had teams come together to run online conferences for PyCon AU, linux.conf.au and DrupalSouth over the past two years, which is a huge testament to the dedication of our volunteers who continued to deliver our world-class events to everyone, even with ever changing and unknown situations.
More recently we have seen DrupalSouth launch their in person conference in October this year, which is the first in person event that we have auspiced since COVID-19 emerged.
This is reflective of our communities’ desire to continue holding events, and to also start meeting in person again, allowing people to catch up with friends and colleagues they have not seen for many months or years.

As we look towards what our next iteration of community events will be, we need to do this while keeping in mind that our society has changed.
Planning physical events, especially our conferences, has always been a challenging task for our volunteers to undertake.
We now have to add in extra unknowns, such as how many people are able, and willing, to travel to attend a conference, how to continue to make the event accessible to as many people as possible, how to minimise the risk of Coronavirus, and more recently, how to juggle increasing costs for running an event while still keeping tickets affordable.

It is with all of this in mind that Linux Australia has decided to organise Everything Open 2023, a new conference that embraces all facets of open technology.
There will be presentations on Linux, open source software, open hardware, standards, formats and documentation, and of course the communities that surround them.
There will be technical deep-dives into all of these topics, while also providing beginner and intermediate level presentations for those who are newer to the subject.
The schedule will be put together based on the session proposals that we receive, after going through review by our Session Selection Committee, whose members come from a wide range of open technology backgrounds.

Over the past few years there have been several discussions around what changes should be made to linux.conf.au in the future.
We have also received ongoing feedback from delegates and their employers, speakers, and sponsors, both potential and actual, which have shown some common themes.
Linux Australia is incorporating constructive suggestions from the community into our future events, starting with Everything Open.
A key part of this is ensuring the event is inclusive and open to everyone, no matter their connection or usage of open technologies.
All of the presentations will have clear topic tags within the schedule, to allow delegates to find presentations in the areas they are most interested in.
There will also be some themed tracks, grouping sets of talks together, to allow communities to come together within the event.

It is also important that we continue to run a grassroots, community focused event in the spirit of linux.conf.au - which is what Everything Open will be.
We will continue to have a volunteer organising committee looking after all of the planning and delivery of the event, with a core team working over the coming months and expanding out to a wider group of volunteers on the ground during the conference.
We will continue to record all presentations and make them freely available to everyone after the event.

We are looking forward to coming together at Everything Open 2023 and delivering another quality conference for our whole community.
We can’t wait to see you there.
