---
layout: page
title: Terms and Conditions
permalink: /about/terms-and-conditions/
sponsors: true
---
<hr>
<span class="abstract">
    All participants of Everything Open events agree to be bound by the Terms and Conditions.
</span>
<hr>

## Registration
Registering for the event does not guarantee your ticket until it has been paid for in full.
Please ensure you pay the registration invoice as soon as possible to secure your ticket.

## Security and credit cards
All transactions are processed by [Stripe](https://stripe.com), and card details are sent over TLS.
The facility accepts Mastercard, Visa, and American Express.
All transactions are performed by the event organisers on behalf of Linux Australia.
Cardholder data is not stored by Linux Australia.

## Cancellation policy
Please review the cancellation policy for each event to determine what refunds, if any, may be available.

## Substitutions
You may substitute another person, however you must contact us with this person's details.
Please note that any personalised items may not be provided if the substitution occurs too close to the event opening date.

## Privacy notice
In the course of registering for the event and related events, personal information will be collected about attendees such as their name, contact details, etc.
This information is required to facilitate registration to to the event, for catering requirements, and for organisers or their agents to contact particular attendees as and when required in respect of the event.
Attendees who do not disclose this personal information will be unable to complete registration at the event and will therefore not be able to attend.

Personal information will only be disclosed to Linux Australia, and to Government agencies where organisers believe disclosure is appropriate for legal compliance and law enforcement; to facilitate court proceedings; to enforce our terms and conditions; or to protect the rights, property, or safety of the event, our attendees, or others.
Linux Australia will not sell your personal information to third parties and will not use your personal information to send promotional material from any of our affiliated partners and/or sponsors.

As part of the registration process attendees will be asked if they would like to subscribe to the event Mailing Lists and/or the event Announce Mailing List.
Attendees who subscribe to one or more of these lists will be sent emails from the event organisers and other subscribers to the Mailing Lists.
If at any time, attendees wish to unsubscribe from any of these Mailing Lists, please follow the 'how to unsubscribe' directions on the bottom of any message you receive from these Mailing Lists.

From time to time event organisers update their information and website practices.
Please regularly review this page for the most recent information about the event privacy practices.

All personal information will be kept private and used only for event registration purposes, statistics for future events, and convenience for future event registration.

## Network
The event may provide attendees in some locations with access to a wired and/or wireless network.
The access to this network is a privilege and not an entitlement, and must be used appropriately.
Inappropriate use includes, but is not limited to: unlawful activities, interfering with the equipment or network access of others and not respecting the reasonable expectations of privacy that attendees have for traffic flowing though the network.

Any deliberately malicious activities on either the wired or wireless networks will be grounds for instant dismissal from the conference (without reimbursement).

If any attendees use the network inappropriately, then the event organisers will take any enforcement action they consider appropriate.
Enforcement action includes but is not limited to:

* suspending access to the network
* disconnecting the network permanently
* the alleged offender may be asked to immediately leave the venue and/or will be prohibited from continuing to attend the conference (without reimbursement)
* the incident may be reported to local or Federal police
* any other measure the event organisers see fit

Beware that for security and operational reasons the event team may both monitor and log network traffic.

## Audio visual
Event organisers may provide recordings of talks (audio and/or video) given at the event.
This service is provided on a best-effort basis only.
Any recordings will be released as and when they are ready, which may be some time after the conclusion of the event, and the recordings may be of varying quality.

## Discrimination and anti-social behaviour
Linux Australia is proud to support diverse groups of people in IT, particularly women, and will not tolerate in any fashion any intimidation, harassment, and/or any abusive, discriminatory or derogatory behaviour by any attendees of the event and/or related events.

Examples of these behaviours and measures the event organisers or Linux Australia may take are set out in the [Code of Conduct](/attend/code-of-conduct/).
By registering for and attending a Linux Australia event, you agree to this [Code of Conduct](/attend/code-of-conduct/).

## Media
There are a limited number of Media Passes available to media personnel.
Media Passes are free of charge, and entitle media personnel to attend the event with all the entitlements of a Professional registration.
Please note, due to the limited numbers of Media Passes available, all Media Passes will need to be approved by the event organisers.

Any media attending the event are required to identify themselves as "media" to attendees prior to speaking on the record with any attendees of the event.
It is the responsibility of the media to introduce themselves to the persons they wish to interview and to arrange any interviews with those persons.
The event organisers will not make introductions or arrange interviews on behalf of media.

To apply for a Media Pass, please contact [contact@everythingopen.au](mailto:contact@everythingopen.au).

## Students
Students who register for a student ticket (if available) to attend the event will be required to provide event organisers with proof that they are eligible for registration as a student, such as providing a valid full time student ID card.

## Alcohol
Liquor licensing laws require persons to be aged 18 years and older before they can lawfully drink alcohol in Australia.
The licensed premises used as event venues will not serve alcohol to persons 18 years and under.
Those attendees who look under 25 years old, may be required to show identification such as a Passport (non-Australian driver licences are generally not accepted) to licensed premises proving they are 18 years old or older.

Those attendees who are younger than 18 years old are required to advise event organisers of their age, when they register.
Those attendees may not be able to attend all of the associated social events.

Please consume alcohol responsibly.
Please note, licensed premises in Australia are prohibited from selling or supplying alcohol to intoxicated persons.
Please adhere to local liquor laws which are available in most licensed establishments.

## Smoke-free
All event venues including the social event venues are smoke-free.
If attendees wish to smoke during the event and/or related events, they must do so in signed areas.
Please consider others and refrain from smoking directly outside the venues' entrances.

## Health and safety
If you are attending the event as a speaker, it is your responsibility to ensure that your talk meets the Health and Safety requirements under Australian law.
If you are unsure, please contact the event organisers on [contact@everythingopen.au](mailto:contact@everythingopen.au) to discuss it further.
