---
layout: page
title: Contact
permalink: /about/contact/
sponsors: true
---

## Contact the Organising Team

Is there something you would like to know that isn't on this site? Please contact us via email.

<i class="bi-envelope-fill"></i> Email: [contact@everythingopen.au](mailto:contact@everythingopen.au)

## Safety

We have a [Code of Conduct](/about/code-of-conduct/) that details the standards of behaviour that we expect from participants at all Everything Open events, and actions that the organising team may take when those standards are breached.

Every participant, including organisers, and contract staff members, have agreed to abide by the terms of this Code of Conduct.

If another participant’s behaviour is making you feel unsafe, uncomfortable, or threatened, you can contact a member of our safety team using one of the following methods:

* <i class="bi-envelope-fill"></i> Email: [safety@everythingopen.au](mailto:safety@everythingopen.au)
* At an event: Go to the registration desk and ask for a member of the safety team.

Our safety team will take steps to de-escalate an incident, take a report from you, and make a recommendation on the course of action.
If you are not comfortable with the first person who responds to your report taking further action, you may ask for a different member of the team.

If you feel that you have not been taken seriously or that an incident was not responded to in a satisfactory way, you can ask for Linux Australia, the parent body of Everything Open, to review the way your report was handled.

## Stay up to date

We have several channels that we use to post announcements in the lead up to, and during, the conference.

* <i class="bi-mastodon"></i> Mastodon: [@EverythingOpen@fosstodon.org](https://fosstodon.org/@EverythingOpen), hashtag #EverythingOpen
* <i class="bi-twitter"></i> Twitter: [@_everythingopen](https://twitter.com/_everythingopen), hashtag #EverythingOpen
* <i class="bi-linkedin"></i> LinkedIn: [Everything Open](https://www.linkedin.com/showcase/everythingopen/)
* <i class="bi-facebook"></i> Facebook: [Everything Open](https://www.facebook.com/EverythingOpenConference/)
* <i class="bi-inboxes-fill"></i> Announce mailing list: [Everything Open Announce](https://lists.linux.org.au/mailman/listinfo/eo-announce)

## Contact Linux Australia

Linux Australia is the auspice for Everything Open.
Check out their website for more details and contact information [linux.org.au](https://linux.org.au)
