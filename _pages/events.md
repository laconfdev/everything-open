---
layout: page
title: Events
permalink: /events/
sponsors: true
---

Everything Open began in 2023 and will be held once a year.

## Adelaide 2025

[Everything Open 2025](https://2025.everythingopen.au/) will be held in Adelaide (Tarntanya), SA, Australia in January 2025.

## Gladstone 2024

[Everything Open 2024](https://2024.everythingopen.au/) was held in Gladstone, QLD, Australia in April 2024.

Keynotes
* Geoff Huston
* Jana Dekanovska
* Professor Aaron Quigley
* Rae Johnston

## Melbourne 2023

[Everything Open 2023](https://2023.everythingopen.au/) was held in Melbourne (Naarm), VIC, Australia in March 2023.

Keynotes
* Seb Chan
* Lyndsey Jackson
* Hugh Blemings
* Rebecca Giblin
